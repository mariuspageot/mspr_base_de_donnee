USE [master]
GO
/****** Object:  Database [build_control]    Script Date: 05/07/2021 13:42:07 ******/
CREATE DATABASE [build_control]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'build_control', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\build_control.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'build_control_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\build_control_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [build_control] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [build_control].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [build_control] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [build_control] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [build_control] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [build_control] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [build_control] SET ARITHABORT OFF 
GO
ALTER DATABASE [build_control] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [build_control] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [build_control] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [build_control] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [build_control] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [build_control] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [build_control] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [build_control] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [build_control] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [build_control] SET  DISABLE_BROKER 
GO
ALTER DATABASE [build_control] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [build_control] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [build_control] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [build_control] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [build_control] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [build_control] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [build_control] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [build_control] SET RECOVERY FULL 
GO
ALTER DATABASE [build_control] SET  MULTI_USER 
GO
ALTER DATABASE [build_control] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [build_control] SET DB_CHAINING OFF 
GO
ALTER DATABASE [build_control] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [build_control] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [build_control] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [build_control] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'build_control', N'ON'
GO
ALTER DATABASE [build_control] SET QUERY_STORE = OFF
GO
USE [build_control]
GO
/****** Object:  Table [dbo].[corp_metier]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[corp_metier](
	[idCorpMetier] [uniqueidentifier] NOT NULL,
	[label] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_corp_metier] PRIMARY KEY CLUSTERED 
(
	[idCorpMetier] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[chantier]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chantier](
	[idChantier] [uniqueidentifier] NOT NULL,
	[label] [nvarchar](50) NOT NULL,
	[date_deb] [date] NOT NULL,
	[date_fin] [date] NULL,
	[gps] [nvarchar](50) NULL,
	[ville] [nvarchar](50) NULL,
	[idEmploye] [uniqueidentifier] NOT NULL,
	[idClient] [uniqueidentifier] NULL,
 CONSTRAINT [PK_chantier] PRIMARY KEY CLUSTERED 
(
	[idChantier] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[incident]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[incident](
	[idIncident] [uniqueidentifier] NOT NULL,
	[label] [nvarchar](50) NOT NULL,
	[idCorpMetier] [uniqueidentifier] NOT NULL,
	[idCapture] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_incident] PRIMARY KEY CLUSTERED 
(
	[idIncident] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[capture]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[capture](
	[reference] [nvarchar](50) NOT NULL,
	[date_capture] [date] NOT NULL,
	[heure] [nvarchar](50) NOT NULL,
	[idChantier] [uniqueidentifier] NOT NULL,
	[source] [nvarchar](250) NOT NULL,
	[gps] [nvarchar](50) NOT NULL,
	[jalon_gant] [uniqueidentifier] NULL,
	[etape_capture] [uniqueidentifier] NULL,
	[idType] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_capture] PRIMARY KEY CLUSTERED 
(
	[reference] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetIncidents]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rose
-- Create date: 
-- Description:	
	/* 
		Développez un deuxième composant logiciel permettant de retrouver les problèmes identifiés
		grâce aux captures faites par les outils de captures.
		Fonction qui perment de récupérer les incidents
		Pensez donc à la gestion des retours d’analyse et aux natures de problèmes relevés 
		rattachés aux corps de métiers impliqués.
	*/
-- =============================================
CREATE FUNCTION [dbo].[GetIncidents]
(
	-- Add the parameters for the function here
	@idChantier uniqueidentifier
)

RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT i.label, cm.label as 'corp métier', ch.label as chantier
	from incident i
	join capture c on i.idCapture = c.reference
	join corp_metier cm on i.idCorpMetier = cm.idCorpMetier
	join chantier ch on c.idChantier = ch.idChantier
	where c.idChantier = @idChantier
)
GO
/****** Object:  Table [dbo].[chantier_sousTraitant]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chantier_sousTraitant](
	[idChanter] [uniqueidentifier] NOT NULL,
	[idSousTraitant] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_chantier_sousTraitant] PRIMARY KEY CLUSTERED 
(
	[idChanter] ASC,
	[idSousTraitant] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[client]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[client](
	[idClient] [uniqueidentifier] NOT NULL,
	[nom] [nchar](10) NOT NULL,
 CONSTRAINT [PK_client] PRIMARY KEY CLUSTERED 
(
	[idClient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[employe]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employe](
	[idEmploye] [uniqueidentifier] NOT NULL,
	[nom] [nvarchar](50) NOT NULL,
	[prenom] [nvarchar](50) NOT NULL,
	[mail] [nvarchar](50) NULL,
	[departement] [nvarchar](50) NULL,
 CONSTRAINT [PK_employe] PRIMARY KEY CLUSTERED 
(
	[idEmploye] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[etape]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[etape](
	[idEtape] [uniqueidentifier] NOT NULL,
	[label] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_etape] PRIMARY KEY CLUSTERED 
(
	[idEtape] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gant]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gant](
	[idGant] [uniqueidentifier] NOT NULL,
	[label] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_gant] PRIMARY KEY CLUSTERED 
(
	[idGant] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sous_traitant]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sous_traitant](
	[idSousTraitant] [uniqueidentifier] NOT NULL,
	[label] [nvarchar](50) NOT NULL,
	[corpsMetier] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_sous_traitant] PRIMARY KEY CLUSTERED 
(
	[idSousTraitant] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[typeCapture]    Script Date: 05/07/2021 13:42:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[typeCapture](
	[idType] [uniqueidentifier] NOT NULL,
	[label] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_typeCapture] PRIMARY KEY CLUSTERED 
(
	[idType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[capture] ([reference], [date_capture], [heure], [idChantier], [source], [gps], [jalon_gant], [etape_capture], [idType]) VALUES (N'#00122', CAST(N'2021-07-01' AS Date), N'8h11', N'8b7d29bb-7f07-408e-95a4-ab226b268971', N'https://my.matterport.com/folders/uP8wSdfuhbC?page=1&ordering=-created&page_size=24&type=all&parent=uP8wSdfuhbC&organization=snn2BCakTPF', N'44.917639, -0.735070
', NULL, NULL, N'c23d05fd-37b1-4757-83ea-7f7b601aaa6b')
INSERT [dbo].[capture] ([reference], [date_capture], [heure], [idChantier], [source], [gps], [jalon_gant], [etape_capture], [idType]) VALUES (N'#00123', CAST(N'2021-07-02' AS Date), N'8h11', N'8b7d29bb-7f07-408e-95a4-ab226b268971', N'https://my.matterport.com/folders/uP8wSdfuhbC?page=1&ordering=-created&page_size=24&type=all&parent=uP8wSdfuhbC&organization=snn2BCakTPF', N'44.917639, -0.735070
', NULL, NULL, N'c23d05fd-37b1-4757-83ea-7f7b601aaa6b')
INSERT [dbo].[capture] ([reference], [date_capture], [heure], [idChantier], [source], [gps], [jalon_gant], [etape_capture], [idType]) VALUES (N'#00234
', CAST(N'2021-05-01' AS Date), N'8h15', N'3d60419b-3433-4691-8f2a-b7cff9ca28b0', N'"09776545.jpg
IG2234.mp4
IG2235.mp4"
', N'44.634074, -1.083128
', NULL, NULL, N'5547f142-edcd-4776-acbc-85c96c4daf54')
GO
INSERT [dbo].[chantier] ([idChantier], [label], [date_deb], [date_fin], [gps], [ville], [idEmploye], [idClient]) VALUES (N'8b7d29bb-7f07-408e-95a4-ab226b268971', N'Lotissement des Anges', CAST(N'2021-01-01' AS Date), NULL, N'44.634074, -1.083128', N'Saint Aubin de Médoc
', N'30415765-68ea-4684-adf0-1ec08e1c6702', NULL)
INSERT [dbo].[chantier] ([idChantier], [label], [date_deb], [date_fin], [gps], [ville], [idEmploye], [idClient]) VALUES (N'3d60419b-3433-4691-8f2a-b7cff9ca28b0', N'Résidence les Capucines
', CAST(N'2021-04-01' AS Date), NULL, N'44.634074, -1.083128
', N'Gujan Mestras
', N'223fba3c-8454-42ba-a8ef-5a438c2e3107', NULL)
GO
INSERT [dbo].[corp_metier] ([idCorpMetier], [label]) VALUES (N'ace4f43e-e4a8-4dd2-a4ec-afeeb36773c9', N'Couverture')
INSERT [dbo].[corp_metier] ([idCorpMetier], [label]) VALUES (N'210023e9-7dce-425d-94db-dbf7a4bedd90', N'Gros Oeuvre')
INSERT [dbo].[corp_metier] ([idCorpMetier], [label]) VALUES (N'a69afcf0-fa4a-4629-8f0c-ef37fc763369', N'Charpente')
GO
INSERT [dbo].[employe] ([idEmploye], [nom], [prenom], [mail], [departement]) VALUES (N'30415765-68ea-4684-adf0-1ec08e1c6702', N'Pakot', N'Mumus', N'mail', N'Administration')
INSERT [dbo].[employe] ([idEmploye], [nom], [prenom], [mail], [departement]) VALUES (N'223fba3c-8454-42ba-a8ef-5a438c2e3107', N'Simpson', N'Steve', N'mail', N'Comptabilité')
GO
INSERT [dbo].[incident] ([idIncident], [label], [idCorpMetier], [idCapture]) VALUES (N'b50f7e5b-ab87-4ca4-933b-0fd353d8a97f', N'Carence rouleau cable 0.2', N'ace4f43e-e4a8-4dd2-a4ec-afeeb36773c9', N'#00122')
INSERT [dbo].[incident] ([idIncident], [label], [idCorpMetier], [idCapture]) VALUES (N'd1d894ed-1093-4bee-9b93-6aceda355482', N'Montage défectueux compteurs', N'210023e9-7dce-425d-94db-dbf7a4bedd90', N'#00123')
INSERT [dbo].[incident] ([idIncident], [label], [idCorpMetier], [idCapture]) VALUES (N'0fd18713-d8ad-4efb-ac7f-86f23650e238', N'Carence ciment fondation Nord-Est', N'ace4f43e-e4a8-4dd2-a4ec-afeeb36773c9', N'#00122')
INSERT [dbo].[incident] ([idIncident], [label], [idCorpMetier], [idCapture]) VALUES (N'434bfaa1-9a97-4b8d-935b-b5dd3ff1d585', N'Montage défectueux compteurs', N'210023e9-7dce-425d-94db-dbf7a4bedd90', N'#00122')
INSERT [dbo].[incident] ([idIncident], [label], [idCorpMetier], [idCapture]) VALUES (N'f2a57a66-3f74-4d42-971b-d3d607c18863', N'Carence rouleau cable 0.2', N'ace4f43e-e4a8-4dd2-a4ec-afeeb36773c9', N'#00234
')
GO
INSERT [dbo].[typeCapture] ([idType], [label]) VALUES (N'c23d05fd-37b1-4757-83ea-7f7b601aaa6b', N'Matterport')
INSERT [dbo].[typeCapture] ([idType], [label]) VALUES (N'5547f142-edcd-4776-acbc-85c96c4daf54', N'Drone')
GO
ALTER TABLE [dbo].[chantier] ADD  CONSTRAINT [DF_chantier_idChantier]  DEFAULT (newid()) FOR [idChantier]
GO
ALTER TABLE [dbo].[chantier_sousTraitant] ADD  CONSTRAINT [DF_chantier_sousTraitant_idChanter]  DEFAULT (newid()) FOR [idChanter]
GO
ALTER TABLE [dbo].[client] ADD  CONSTRAINT [DF_client_idClient]  DEFAULT (newid()) FOR [idClient]
GO
ALTER TABLE [dbo].[corp_metier] ADD  CONSTRAINT [DF_corp_metier_idCorpMetier]  DEFAULT (newid()) FOR [idCorpMetier]
GO
ALTER TABLE [dbo].[employe] ADD  CONSTRAINT [DF_employe_idEmploye]  DEFAULT (newid()) FOR [idEmploye]
GO
ALTER TABLE [dbo].[etape] ADD  CONSTRAINT [DF_etape_idEtape]  DEFAULT (newid()) FOR [idEtape]
GO
ALTER TABLE [dbo].[gant] ADD  CONSTRAINT [DF_gant_idGant]  DEFAULT (newid()) FOR [idGant]
GO
ALTER TABLE [dbo].[incident] ADD  CONSTRAINT [DF_incident_idIncident]  DEFAULT (newid()) FOR [idIncident]
GO
ALTER TABLE [dbo].[sous_traitant] ADD  CONSTRAINT [DF_sous_traitant_idSousTraitant]  DEFAULT (newid()) FOR [idSousTraitant]
GO
ALTER TABLE [dbo].[typeCapture] ADD  CONSTRAINT [DF_typeCapture_idType]  DEFAULT (newid()) FOR [idType]
GO
ALTER TABLE [dbo].[capture]  WITH CHECK ADD  CONSTRAINT [FK_capture_chantier] FOREIGN KEY([idChantier])
REFERENCES [dbo].[chantier] ([idChantier])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[capture] CHECK CONSTRAINT [FK_capture_chantier]
GO
ALTER TABLE [dbo].[capture]  WITH CHECK ADD  CONSTRAINT [FK_capture_etape] FOREIGN KEY([etape_capture])
REFERENCES [dbo].[etape] ([idEtape])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[capture] CHECK CONSTRAINT [FK_capture_etape]
GO
ALTER TABLE [dbo].[capture]  WITH CHECK ADD  CONSTRAINT [FK_capture_gant] FOREIGN KEY([jalon_gant])
REFERENCES [dbo].[gant] ([idGant])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[capture] CHECK CONSTRAINT [FK_capture_gant]
GO
ALTER TABLE [dbo].[capture]  WITH CHECK ADD  CONSTRAINT [FK_capture_typeCapture] FOREIGN KEY([idType])
REFERENCES [dbo].[typeCapture] ([idType])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[capture] CHECK CONSTRAINT [FK_capture_typeCapture]
GO
ALTER TABLE [dbo].[chantier]  WITH CHECK ADD  CONSTRAINT [FK_chantier_client] FOREIGN KEY([idClient])
REFERENCES [dbo].[client] ([idClient])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[chantier] CHECK CONSTRAINT [FK_chantier_client]
GO
ALTER TABLE [dbo].[chantier]  WITH CHECK ADD  CONSTRAINT [FK_chantier_employe] FOREIGN KEY([idEmploye])
REFERENCES [dbo].[employe] ([idEmploye])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[chantier] CHECK CONSTRAINT [FK_chantier_employe]
GO
ALTER TABLE [dbo].[chantier_sousTraitant]  WITH CHECK ADD  CONSTRAINT [FK_chantier_sousTraitant_chantier] FOREIGN KEY([idChanter])
REFERENCES [dbo].[chantier] ([idChantier])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[chantier_sousTraitant] CHECK CONSTRAINT [FK_chantier_sousTraitant_chantier]
GO
ALTER TABLE [dbo].[chantier_sousTraitant]  WITH CHECK ADD  CONSTRAINT [FK_chantier_sousTraitant_sous_traitant] FOREIGN KEY([idSousTraitant])
REFERENCES [dbo].[sous_traitant] ([idSousTraitant])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[chantier_sousTraitant] CHECK CONSTRAINT [FK_chantier_sousTraitant_sous_traitant]
GO
ALTER TABLE [dbo].[incident]  WITH CHECK ADD  CONSTRAINT [FK_incident_capture] FOREIGN KEY([idCapture])
REFERENCES [dbo].[capture] ([reference])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[incident] CHECK CONSTRAINT [FK_incident_capture]
GO
ALTER TABLE [dbo].[incident]  WITH CHECK ADD  CONSTRAINT [FK_incident_corp_metier] FOREIGN KEY([idCorpMetier])
REFERENCES [dbo].[corp_metier] ([idCorpMetier])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[incident] CHECK CONSTRAINT [FK_incident_corp_metier]
GO
USE [master]
GO
ALTER DATABASE [build_control] SET  READ_WRITE 
GO
