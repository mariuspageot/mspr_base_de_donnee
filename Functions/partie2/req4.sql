select count(*) as nbSousTraitants,cm.label
from sous_traitant 
join "chantier_sousTraitant" csT on sous_traitant."idSousTraitant" = csT."idSousTraitant"
join chantier c on csT.idChantier = c.idChantier
join corp_metier cm on sous_traitant.idCorpsMetier = cm.idCorpMetier
group by cm.label
