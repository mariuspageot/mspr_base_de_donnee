declare @ref varchar(10)
select @ref = '#00122'

select capture.date_capture,capture.source,
    (select label from chantier where chantier."idChantier" = capture.idChantier) as chantier,
    cm.label as corpMetier
from capture
    join etape e on capture.etape_capture = e."idEtape"
    join "etape_coprMetier" ecM on e."idEtape" = ecM."idEtape"
    join corp_metier cm on ecM.idCorpMetier = cm."idCorpMetier"
where capture.reference = @ref
