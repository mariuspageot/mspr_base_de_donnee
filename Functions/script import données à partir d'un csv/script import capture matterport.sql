drop table if exists temp
CREATE TABLE temp(
        reference nvarchar(250) NULL,
        date date NULL,
		heure nvarchar(250) NULL,
		chantier nvarchar(250) NULL,
		projet nvarchar(250) NULL,
		url nvarchar(1000) NULL,
		incidents nvarchar(250) NULL,
		gps nvarchar(250) NULL,
		sous_traitants nvarchar(250) NULL,
		jalonGant nvarchar(250) NULL,
		EtapeCapture varchar(250) NULL,
)

BULK INSERT temp
FROM 'D:\cours\MSPR\MSPR3\csv\octobre2020-export-matterport.csv'
WITH (FIRSTROW = 2,
    FIELDTERMINATOR = ';',
    ROWTERMINATOR='\n',
    TABLOCK)


declare db_cursor cursor for
select reference,date,heure,projet,jalonGant,EtapeCapture,url,incidents,gps from temp

declare @Reference nvarchar(250)
declare @Date date
declare @Heure nvarchar(250)
declare @Source nvarchar(250)
declare @GPS nvarchar(250)
declare @Incidents nvarchar(250)

declare @Projet nvarchar(250)
declare @IdProjet nvarchar(250)

declare @JalonGant nvarchar(250)
declare @IdJalonGant nvarchar(250)

declare @EtapeCapture nvarchar(250)
declare @IdEtapeCapture nvarchar(250)

open db_cursor
fetch next from db_cursor into @Reference,@Date,@Heure,@Projet,@JalonGant,@EtapeCapture,@Source,@Incidents,@GPS

while @@FETCH_STATUS = 0
begin 
	select @IdProjet = idChantier from chantier where label = @Projet
	select @IdJalonGant = idGant from gant where label = @JalonGant
	select @IdEtapeCapture = idEtape from etape where label = @EtapeCapture

	insert into capture (reference,date_capture,heure,idChantier,source,gps,jalon_gant,etape_capture,idType) values (@Reference,@Date,@Heure,@IdProjet,@Source,@GPS,@IdJalonGant,@IdEtapeCapture,(select top 1 idType from typeCapture where label = 'Matterport'))

	if @Incidents <> ''
		insert into incident (label,idCapture) values (@Incidents,(select top 1 reference from capture where source = @Source))

	FETCH NEXT FROM db_cursor INTO @Reference,@Date,@Heure,@Projet,@JalonGant,@EtapeCapture,@Source,@Incidents,@GPS
end

close db_cursor
DEALLOCATE db_cursor

select * from capture
