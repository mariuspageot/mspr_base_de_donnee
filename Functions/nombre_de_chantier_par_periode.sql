CREATE OR ALTER FUNCTION nombre_de_chantier_par_periode(@nom nvarchar(50), @coordonnee nvarchar(50), @sous_traitant nvarchar(50), @date_debut date, @date_fin date)
RETURNS TABLE
AS
RETURN
(
   select count(*) as result, @nom as nom
    from chantier
    join "chantier_sousTraitant" csT on chantier."idChantier" = csT."idChantier"
    join sous_traitant st on csT."idSousTraitant" = st."idSousTraitant"
    where chantier.date_deb between @date_debut and @date_fin
   and chantier.label=@nom
   and chantier.gps=@coordonnee
   and st.label=@sous_traitant
);
go