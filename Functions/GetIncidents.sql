USE [build_control]
GO

/****** Object:  UserDefinedFunction [dbo].[GetIncidents]    Script Date: 06/07/2021 20:07:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Rose
-- Create date: 
-- Description:	
	/* 
		D�veloppez un deuxi�me composant logiciel permettant de retrouver les probl�mes identifi�s
		gr�ce aux captures faites par les outils de captures.
		Fonction qui perment de r�cup�rer les incidents
		Pensez donc � la gestion des retours d�analyse et aux natures de probl�mes relev�s 
		rattach�s aux corps de m�tiers impliqu�s.
	*/
-- =============================================
CREATE OR ALTER   FUNCTION [dbo].[GetIncidents]
(
	-- Add the parameters for the function here
	@idChantier uniqueidentifier
)

RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT DISTINCT t.label as Type_incident, cm.label, count(*) as incidence
	from incident i
	join capture c on i.idCapture = c.reference
	join corp_metier cm on i.idCorpMetier = cm.idCorpMetier
	join chantier ch on c.idChantier = ch.idChantier
	join typeIncident t on i.idTypeIncident = t.idTypeIncident
	where c.idChantier = @idChantier
	group by t.label, cm.label
)
GO


