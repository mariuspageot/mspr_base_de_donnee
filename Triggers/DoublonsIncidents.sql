USE [build_control]
GO

/****** Object:  Trigger [dbo].[DoublonsIncidents]    Script Date: 06/07/2021 11:01:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER TRIGGER [dbo].[DoublonsIncidents] 
   ON  [dbo].[incident]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Instancier les variables
	declare @label_incident nvarchar(50), @chantier nvarchar(50), @heure nvarchar(50), @date date, @corp_metier nvarchar(50), @idCapture nvarchar(50)
	declare @errmsg nvarchar(max)

	-- Récupérer les informations insérées dans des variables
	SELECT @label_incident=i.label, @chantier = ch.label, @heure = c.heure, @date = c.date_capture, @corp_metier = cm.label, @idCapture = c.reference
	from incident i
	join inserted on inserted.idIncident = i.idIncident
	join capture c on i.idCapture = c.reference
	join corp_metier cm on i.idCorpMetier = cm.idCorpMetier
	join chantier ch on c.idChantier = ch.idChantier
	where i.idIncident = inserted.idIncident
	-- Vérifier si un élément avec les mêmes données existe déja
	IF EXISTS (
		SELECT 1
		FROM incident i
		join capture c on i.idCapture = c.reference
		join corp_metier cm on i.idCorpMetier = cm.idCorpMetier
		join chantier ch on c.idChantier = ch.idChantier
        WHERE i.label = @label_incident
		AND i.idCapture = @idCapture
		AND ch.label = @chantier
		AND cm.label = @corp_metier
		)
	BEGIN
	SET @errmsg = CONCAT('Il existe déja un incident "',@label_incident,'" sur le chantier "', @chantier,'" à ',@heure,' impliquant ',@corp_metier,' sur la capture ',@idCapture)
	RAISERROR (@errmsg, 16, 1);
	ROLLBACK TRANSACTION
	END

END
GO

ALTER TABLE [dbo].[incident] ENABLE TRIGGER [DoublonsIncidents]
GO

