USE [build_control]
GO

/****** Object:  Table [dbo].[typeIncident]    Script Date: 06/07/2021 11:45:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[typeIncident](
	[idTypeIncident] [uniqueidentifier] NOT NULL,
	[label] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_typeIncident] PRIMARY KEY CLUSTERED 
(
	[idTypeIncident] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


