/* Pour éviter les problèmes éventuels de perte de données, passez attentivement ce script en revue avant de l'exécuter en dehors du contexte du Concepteur de bases de données.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.chantier_sousTraitant.idChanter', N'Tmp_idChantier', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.chantier_sousTraitant.Tmp_idChantier', N'idChantier', 'COLUMN' 
GO
ALTER TABLE dbo.chantier_sousTraitant SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.chantier_sousTraitant', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.chantier_sousTraitant', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.chantier_sousTraitant', 'Object', 'CONTROL') as Contr_Per 