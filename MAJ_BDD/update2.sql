/* Pour éviter les problèmes éventuels de perte de données, passez attentivement ce script en revue avant de l'exécuter en dehors du contexte du Concepteur de bases de données.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.corp_metier SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.corp_metier', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.corp_metier', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.corp_metier', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.etape SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.etape', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.etape', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.etape', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.etape_coprMetier
	(
	idCorpMetier uniqueidentifier NOT NULL,
	idEtape uniqueidentifier NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.etape_coprMetier ADD CONSTRAINT
	IX_etape_coprMetier UNIQUE NONCLUSTERED 
	(
	idCorpMetier,
	idEtape
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.etape_coprMetier ADD CONSTRAINT
	FK_etape_coprMetier_etape FOREIGN KEY
	(
	idEtape
	) REFERENCES dbo.etape
	(
	idEtape
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.etape_coprMetier ADD CONSTRAINT
	FK_etape_coprMetier_corp_metier FOREIGN KEY
	(
	idCorpMetier
	) REFERENCES dbo.corp_metier
	(
	idCorpMetier
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.etape_coprMetier SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.etape_coprMetier', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.etape_coprMetier', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.etape_coprMetier', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.sous_traitant.corpsMetier', N'Tmp_idCorpsMetier', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.sous_traitant.Tmp_idCorpsMetier', N'idCorpsMetier', 'COLUMN' 
GO
ALTER TABLE dbo.sous_traitant ADD CONSTRAINT
	FK_sous_traitant_corp_metier FOREIGN KEY
	(
	idCorpsMetier
	) REFERENCES dbo.corp_metier
	(
	idCorpMetier
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.sous_traitant SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.sous_traitant', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.sous_traitant', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.sous_traitant', 'Object', 'CONTROL') as Contr_Per 